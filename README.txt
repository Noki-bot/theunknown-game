# README #

This is the final build of The Unknown

The Github with all commits can be found here: https://github.com/nok-bot/The_Unknown_/commits/master
The itch.io page can be found here: https://kakerlake.itch.io/the-unknown

### Contributors ###

Jana Stoerbrauck:
https://nokk.itch.io/
https://www.artstation.com/nokk

Daniel Stoerbrauck:
https://stoerbi.itch.io/
https://www.artstation.com/duskten
